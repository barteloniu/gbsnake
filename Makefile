BUILDDIR := build
EMULATOR := bgb  # sadly proprietary

all: $(BUILDDIR)/snake.gb

$(BUILDDIR)/snake.gb: $(BUILDDIR)/main.o
	rgblink -m $(BUILDDIR)/snake.map -n $(BUILDDIR)/snake.sym -o $(BUILDDIR)/snake.gb $(BUILDDIR)/main.o
	rgbfix -v -p 0 $(BUILDDIR)/snake.gb  # pad the rom, add the logo etc.

$(BUILDDIR)/main.o: src/main.asm $(BUILDDIR)/tiles.bin
	rgbasm -i src/ -i $(BUILDDIR)/ -o $(BUILDDIR)/main.o src/main.asm

$(BUILDDIR)/tiles.bin: images/tiles.png
	mkdir -p $(BUILDDIR)
	python3 tools/png2tiles.py images/tiles.png $(BUILDDIR)/tiles.bin

run: $(BUILDDIR)/snake.gb
	$(EMULATOR) $(BUILDDIR)/snake.gb

# delete the last build, recompile and run in the emulator
dev: clean run

clean:
	rm -r $(BUILDDIR)
