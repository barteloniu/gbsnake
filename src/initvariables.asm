PowerOnInitVars:
	; certain variables shouldn't be reset when restarting
	xor a
	ld [ButtonsPressed], a
	ld [Weyl], a

InitVariables:
	; reset variables
	xor a  ; a = 0
	ld [VBlankFlag], a
	ld [FrameCounter], a
	ld [ButtonsPressedNow], a
	ld [Grow], a

	; clean MapData
	ld d, a  ; source = 0
	ld hl, MapData  ; destination
	ld bc, $20 * $20  ; counter
	call MemSet

	; set the position of the head and the tail
	ld hl, MAP_CENTER
	ld a, h
	ld [HeadPosition], a
	ld [OldHeadPosition], a
	ld [TailPosition], a
	ld [OldTailPosition], a
	ld a, l
	ld [TailPosition+1], a
	ld [OldTailPosition+1], a
	inc a
	ld [HeadPosition+1], a
	ld [OldHeadPosition+1], a

	ld hl, MapData
	ld a, h
	ld [FoodPosition], a
	ld [OldFoodPosition], a
	ld a, l
	ld [FoodPosition+1], a
	ld [OldFoodPosition+1], a

	; set all direction related stuff
	ld a, 1
	ld [Direction], a

	ld a, [HeadPosition]
	ld h, a
	ld a, [HeadPosition+1]
	ld l, a
	ld a, 1
	ld [hl], a

	ld a, [TailPosition]
	ld h, a
	ld a, [TailPosition+1]
	ld l, a
	ld a, 1
	ld [hl], a
	
	; set the seed if we are unlucky or using a crappy emulator
	ld a, [RandomSeed]
	and a
	jr nz, .seedisfine
	ld a, $69  ;  lol xd haha
	ld [RandomSeed], a
.seedisfine
	ld [Random], a

.return
	ret
