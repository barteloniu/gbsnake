GameLoop:
	halt  ; stop system clock
	nop  ; this bug thingy

	; check if it was a VBlank interrupt
	ld a, [VBlankFlag]
	and a
	jp z, GameLoop  ; if not, go back

	xor a
	ld [VBlankFlag], a  ; reset the flag

	call GetInput

	call ChangeState

	; check which state we are at
	ld a, [GameState]
	cp STATE_PLAYING
	jr nz, GameLoop  ; don't do anything if not playing

	call ChangeDirection

	call Move

	jr GameLoop
