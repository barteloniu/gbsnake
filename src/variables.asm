VBlankFlag:			ds 1
FrameCounter:		ds 1
ButtonsPressed:		ds 1
ButtonsPressedNow:	ds 1
Direction:			ds 1  ; 0: right, 1: up, 2: left, 3: down
MapData:			ds $20 * $20
HeadPosition:		ds 2
OldHeadPosition:	ds 2
TailPosition:		ds 2
OldTailPosition:	ds 2
FoodPosition:		ds 2
OldFoodPosition:	ds 2
Random:				ds 1
Weyl:				ds 1
RandomSeed:			ds 1
Grow:				ds 1
GameState:			ds 1

GetTilePos: MACRO
; start, x, y
POS = \1 + \2 + $20 * \3
ENDM
