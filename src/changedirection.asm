CheckTurn:
	; read the current direction
	ld a, [HeadPosition]
	ld h, a
	ld a, [HeadPosition+1]
	ld l, a
	ld a, [hl]

	; check if we want to turn back
	cp b
	jr nz, .return  ; if not continue

	pop hl  ; otherwise skip the rest and return from ChangeDirection

.return
	ret
	

ChangeDirection:
	ld a, [ButtonsPressedNow]
	ld b, a  ; make a backup

	; switch
	and BUTTON_RIGHT
	jr nz, .right
	ld a, b
	and BUTTON_LEFT
	jr nz, .left
	ld a, b
	and BUTTON_UP
	jr nz, .up
	ld a, b
	and BUTTON_DOWN
	jr nz,  .down
	ret


; change the rotation, but first make sure we won't turn back	
.right
	ld b, 3
	call CheckTurn
	ld a, 1
	ld [Direction], a
	ret
.up
	ld b, 4
	call CheckTurn
	ld a, 2
	ld [Direction], a
	ret
.left
	ld b, 1
	call CheckTurn
	ld a, 3
	ld [Direction], a
	ret
.down
	ld b, 2
	call CheckTurn
	ld a, 4
	ld [Direction], a
	ret

.return
	ret
