Multiply:
	; FIRST: a
	; SECOND: hl
	; RESULT: bc

; ancient egyptian / peasant multiplication
; is it faster than simply adding x y times? i have no idea.
; is it cooler? of course it is!

	ld bc, 0
	and a  ; reset the carry flag
.loop
	rra  ; rotate a right through carry
	jr nc, .dontadd  ; first bit was 0
	ld d, a  ; backup
	; bc += hl
	ld a, c
	add l
	ld c, a
	ld a, b
	adc h
	ld b, a
	ld a, d  ; load the backup
.dontadd
	add hl, hl

	and a  ; check if zero and reset carry for the next run
	jr nz, .loop

	ret


Randomise:
; OUTPUT: a
; Middle Square Weyl Sequence PRNG
	; square
	ld a, [Random]
	ld h, 0
	ld l, a
	call Multiply  ; OUT: bc

	; weyl
	ld a, [RandomSeed]
	ld d, a
	ld a, [Weyl]
	add a, d
	ld [Weyl], a
	ld d, a
	; add it
	ld a, c
	add d
	ld c, a
	ld a, b
	adc 0
	; ld b, a ;  not used

	; middle
	; shift left 4 bits
REPT 4
	sla a
ENDR
	; shift right 4 bits
REPT 4
	srl c
ENDR
	or c

	ld [Random], a  ; save the result

	ret

