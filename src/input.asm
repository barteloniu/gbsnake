GetInput:
	; save the previous state into d
	ld a, [ButtonsPressed]
	ld d, a
	
	; turn the d-pad on
	ld a, %00100000
	ld [rP1], a

	; wait a few cycles and read d-pad data
	REPT 3
	ld a, [rP1]
	ENDR
	and %00001111  ; we only care about the lower nibble
	swap a  ; swap the nibbles around
	ld b, a

	; switch to the rest of the buttons
	ld a, %00010000
	ld [rP1], a

	; wait another few cycles and read button data
	REPT 6
	ld a, [rP1]
	ENDR
	and %00001111
	or b  ; combine the reads
	cpl  ; now 1 means pressed

	ld [ButtonsPressed], a  ; store the new state
	ld c, a  ; make a copy

	; reset the joypad
	ld a, %00110000
	ld [rP1], a

	; see which buttons have started to be pressed
	ld a, c
	xor d  ; now we see which ones have changed
	and c  ; get only the pressed ones
	ld [ButtonsPressedNow], a

	ret
