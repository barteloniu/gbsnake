Start:
	call PowerOnInitVars  ; certain variables shouldn't be reset when restarting
	di  ; disable interrupts
	ld sp, $fffe  ; move the stack pointer to the top of the high ram

	; turn off the lcd
	call WaitForVBlank

	xor a  ; a = 0
	ld [rLCDC], a  ; turn the LCD off
	
	; copy the tiles
	ld hl, TILE_DATA_START  ; the beginning of the tile data table
	ld de, Tiles  ; source
	ld bc, TilesEnd - Tiles  ; counter
	call MemCopy

	; set the window tiles
	ld hl, WIN_START
	ld d, 0  ; clean
	ld bc, $20 * $20  ; the intire thing
	call MemSet

	; pause text
	GetTilePos WIN_START, 10-3, 5  ; start, x, y
	ld hl, POS
	ld de, PauseStr
	call StringPrint
	
	; continue text
	GetTilePos WIN_START, 10-4, 11
	ld hl, POS
	ld de, ContinueStr
	call StringPrint


	; set the bg palette
	ld a, NORMAL_PALETTE
	ld [rBGP], a

	; set the scroll and window position
	xor a  ; a = 0
	ld [rSCX], a
	ld [rSCY], a
	ld [rWY], a
	ld a, 7
	ld [rWX], a  ; otherwise doesn't work or something

	; disable sound
	ld [rNR52], a

	; enable only VBlank interrupts
	ld a, %00000001
	ld [rIE], a

	call Menu
