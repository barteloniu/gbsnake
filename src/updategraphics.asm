WaitForVBlank:
	; check if the screen is turned off
	ld a, [rLCDC]
	and a
	jr z, .return  ; if so don't check LY

.loop
	ld a, [rLY]
	cp 144 ; the screen is 144 pixels tall
	jr c, .loop

.return
	ret

UpdateMapTile: MACRO
	; redraw the tile
	ld a, [\1]
	ld h, a
	ld a, [\1+1]
	ld l, a
	ld a, [hl]
	; how much we have to add to convert a map position to a VRAM position
	ld bc, $10000 - MapData + BG_START
	add hl, bc
	add a, \2  ; add to change the tile to e.g. a head
	ld [hl], a
	ENDM

UpdateGraphics:
	; let the rest know that a VBlank interrupt has been triggered
	ld a, 1
	ld [VBlankFlag], a

	; only update the snake if playing
	ld a, [GameState]
	cp STATE_PLAYING
	jr nz, .return

	UpdateMapTile OldFoodPosition, 0
	UpdateMapTile FoodPosition, 0
	UpdateMapTile OldHeadPosition, 0
	UpdateMapTile HeadPosition, 4
	UpdateMapTile OldTailPosition, 0
	UpdateMapTile TailPosition, 0

.return
	ret
