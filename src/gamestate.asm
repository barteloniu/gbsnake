Menu:
	ld a, STATE_MENU
	ld [GameState], a
	
	call WaitForVBlank  ; we can only turn off the screen during VBlank
	di  ; disable interrupts. may be unnecessary
	; turn off the LCD
	xor a
	ld [rLCDC], a

	ld a, NORMAL_PALETTE
	ld [rBGP], a

	; reset the background tiles
	ld hl, BG_START  ; top-left corner
	ld d, 0  ; clean everything
	ld bc, $20 * $20  ; $20 rows $20 columns wide
	call MemSet

	; title
	GetTilePos BG_START, 4, 4
	ld hl, POS
	ld de, SnakeStr
	call StringPrint

	; play text
	GetTilePos BG_START, 5, 11
	ld hl, POS
	ld de, PlayStr
	call StringPrint

	; author text
	GetTilePos BG_START, 20-11, 17
	ld hl, POS
	ld de, AuthorStr
	call StringPrint

	; draw a snake on the side
	GetTilePos BG_START, 20-5, 3
	ld hl, POS
	ld [hl], TILE_FOOD
	; add some space
	ld bc, $20 * 3  ; three rows $20 tiles wide
	add hl, bc
	ld [hl], 6  ; head facing up
	ld a, 6  ; length of the body
	ld bc, $20  ; we want to add the width of a row
.loop
	add hl, bc  ; move down
	ld [hl], 2  ; body facing up
	dec a  ; decrement the counter
	; check if we are done
	and a
	jr nz, .loop

	; turn the LCD on and display the background
	ld a, LCD_ON
	ld [rLCDC], a

	reti

Play:
	ld a, STATE_PLAYING
	ld [GameState], a

	call WaitForVBlank
	di

	; turn off the LCD
	xor a
	ld [rLCDC], a

	ld a, NORMAL_PALETTE
	ld [rBGP], a

	; reset the background tiles
	ld hl, BG_START  ; top-left corner
	ld d, 0  ; clean everything
	ld bc, $20 * $20  ; $20 rows $20 columns wide
	call MemSet

	; set up the snake position
	; and all the other variables needed for playing
	call InitVariables
	call NewFood

	; turn on the screen
	ld a, LCD_ON
	ld [rLCDC], a

	reti


Resume:
	ld a, STATE_PLAYING
	ld [GameState], a
	; turn off the end screen
	ld a, [rLCDC]
	; xor swtiches
	xor %00100000  ; bit 5 decides whether the window should be drawn
	ld [rLCDC], a
	; make it dark
	ld a, NORMAL_PALETTE
	ld [rBGP], a

	ret


GameOver:
	ld a, STATE_GAMEOVER
	ld [GameState], a
	
	call WaitForVBlank  ; we can only turn off the screen during VBlank
	di  ; disable interrupts. may be unnecessary
	; turn off the LCD
	xor a
	ld [rLCDC], a

	; make it dark
	ld a, REVERSED_PALETTE
	ld [rBGP], a
	
	; reset the background tiles
	ld hl, BG_START  ; top-left corner
	ld d, 0  ; clean everything
	ld bc, $20 * $20  ; $20 rows $20 columns wide
	call MemSet

	; game over text
	GetTilePos BG_START, 10-2, 4
	ld hl, POS
	ld de, GameOverStr
	call StringPrint

	; replay text
	GetTilePos BG_START, 10-3, 12
	ld hl, POS
	ld de, ReplayStr
	call StringPrint

	; turn on the screen
	ld a, LCD_ON
	ld [rLCDC], a

	reti


Pause:
	ld a, STATE_PAUSE
	ld [GameState], a
	; turn on the end screen
	ld a, [rLCDC]
	; or enbles
	or %00100000  ; bit 5 decides whether the window should be drawn
	ld [rLCDC], a
	; make it dark
	ld a, REVERSED_PALETTE
	ld [rBGP], a

	ret


ChangeState:
	; check if the a state changing button has been pressed

	; check if the game is already paused
	ld a, [GameState]
	cp STATE_MENU
	jr z, .menu
	cp STATE_PLAYING
	jr z, .playing
	cp STATE_GAMEOVER
	jr z, .gameover
	cp STATE_PAUSE
	jr z, .paused

.menu
	ld a, [ButtonsPressedNow]
	and BUTTON_START | BUTTON_A  ; both buttons work
	jr z, .return
	call Play

.playing
	; check if the start button is pressed
	ld a, [ButtonsPressedNow]
	and BUTTON_START
	jr z, .return  ; if not do nothing
	call Pause
	ret

.gameover
	ld a, [ButtonsPressedNow]
	and BUTTON_START | BUTTON_A
	jr z, .return
	call Play

.paused
	ld a, [ButtonsPressedNow]
	and BUTTON_START | BUTTON_A
	jr z, .return
	call Resume

.return
	ret

