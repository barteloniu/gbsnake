CheckTile:
	; only use for the head

	; check if we hit the edge

	; left and right
	ld a, l  ; only the second byte matters
	; subract the second byte of MapData to align
	ld b, MapData & $FF
	sub b
	and 31  ; rows are 32 tiles wide
	cp 20  ; check if we are past 19
	jr c, .top  ; if not carry on
	call GameOver  ; otherwise end

.top
	; check if hl is smaller than the top-left corner without modifying hl
	ld a, l
	sub MapData & $FF  ; second byte
	ld a, h
	sbc MapData >> 8  ; first byte
	jr nc, .bottom  ; >=, carry on
	call GameOver

.bottom
	; check if we are part tile $240 (18 * $20)
	ld a, l
	sub MAP_END & $FF  ; second byte
	ld a, h
	sbc MAP_END >> 8  ; first byte
	jr c, .food  ; smaller, carry on
	call GameOver

.food
	ld a, [hl]
	cp TILE_FOOD
	jr nz, .body

	call NewFood

	; let the move function know that the snake should grow
	ld a, 1
	ld [Grow], a
	ret

.body
	; must be between 1 to 4 inclusive
	cp 1
	jr c, .return
	cp 5
	jr nc, .return

	call GameOver


.return
	ret
