MoveTile: MACRO
	ld a, [\1]
	ld [\2], a
	ld h, a
	ld a, [\1+1]
	ld [\2+1], a
	ld l, a

	; switch
	ld a, \3
	ld [hl], \4  ; update the old tile
	cp 2
	jr z, .up\@
	cp 3
	jr z, .left\@
	cp 4
	jr z, .down\@

; bc is how much we have to add to the address to move in the desired direction
; a row is $20 columns wide
.right\@
	ld bc, 1
	jr .add\@
.up\@
	ld bc, -$20
	jr .add\@
.left\@
	ld bc, -1
	jr .add\@
.down\@
	ld bc, $20

.add\@
	add hl, bc  ; move

	; we don't want the tail to override data nor check for collisions
	ld a, \4
	and a
	jr z, .save\@
	push af
	push hl
	call CheckTile
	pop hl
	pop af

	ld [hl], a  ; save the direction
	; save the new address
.save\@
	ld a, h
	ld [\1], a
	ld a, l
	ld [\1+1], a
ENDM

Move:
	; move every 10 frames
	ld a, [FrameCounter]
	inc a
	ld [FrameCounter], a
	cp 10
	jp nz, .return

	; reset the counter
	xor a
	ld [FrameCounter], a

	;			new tile		old tile	direction	old tile value
	MoveTile HeadPosition, OldHeadPosition, [Direction], a

	; don't move the tail if food has been eaten
	ld a, [Grow]
	and a
	jp nz, .reset

	MoveTile TailPosition, OldTailPosition, [hl], 0

.reset
	xor a
	ld [Grow], a  ; reset

.return
	ret
