NewFood:
	; create new food

	; get a suitable random number
	; between 0 and $23f
	; with column numbers less than 20
.firstbyte
	call Randomise
	; get number smaller than 3
	and 3  ; only first 2 bytes
	cp 3  ; we don't want 3
	jp z, .firstbyte

	ld e, a  ; e is not used by Randomise
.secondbyte
	call Randomise
	ld c, a
	; we don't want a row position (which are 32 tiles wide) past 19
	and 31
	cp 20
	jp nc, .secondbyte
	; check if we are >= $240 (18 * $20)
	ld a, e
	cp 2
	jp nz, .fine  ; first byte smaller than 2, so no worries
	ld a, c
	cp $40
	jp nc, .secondbyte  ; >= $240, try again


.fine
	ld b, e

	; load the current position and update the old one
	ld a, [FoodPosition]
	ld [OldFoodPosition], a
	ld h, a
	ld a, [FoodPosition+1]
	ld [OldFoodPosition+1], a
	ld l, a

	ld [hl], 0  ; delete the previous one

	; new position
	ld hl, MapData
	add hl, bc

	; check if the chosen tile is empty
	ld a, [hl]
	and a  ; 0 means empty
	jr nz, .firstbyte  ; if not choose another

	ld [hl], TILE_FOOD  ; place the new food

	; save the new position
	ld a, h
	ld [FoodPosition], a
	ld a, l
	ld [FoodPosition+1], a

	ret
