# Information about images

1. The tilemap has to be 128x128 pixels
2. Which colours to use

	| number in the pallet | colour in the image |
	| --------------------:| ------------------- |
	| 0                    | #ffffff             |
	| 1                    | #bbbbbb             |
	| 2                    | #444444             |
	| 3                    | #000000             |
