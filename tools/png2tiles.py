import os
import sys
import PIL.Image

# usage: python3 png2tiles.py INPUT_FILE OUTPUT_FILE

colours = [
    (0xff, 0xff, 0xff),
    (0xbb, 0xbb, 0xbb),
    (0x44, 0x44, 0x44),
    (0, 0, 0)
]

with open(sys.argv[2], "wb") as f:
    img = PIL.Image.open(sys.argv[1]).convert(mode="RGB")
    for ty in range(16):
        for tx in range(16):
            for y in range(8):
                b = bytearray(2)
                for x in range(8):
                    index = colours.index(img.getpixel((tx * 8 + x, ty * 8 + y)))
                    b[0] += 2 ** (7-x) * (index % 2)
                    b[1] += 2 ** (7-x) * (index // 2)
                f.write(b)
